﻿using dekodlogg.interfaces;
using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.live {
        class SeriePortLive : ILive {
                SerialPort sp;
                IModel model;
                int sensors = 0;
                //DateTime start;
                FileStream liveFile;
                PakkeDekoder pakkeDekoder;

                public SeriePortLive(string serieport, int baudrate) {
                        pakkeDekoder = new PakkeDekoder(processPackage);
                        sp = new SerialPort(serieport, baudrate, Parity.None, 8, StopBits.One);
                        sp.DataReceived += Sp_DataReceived;
                        model = null;
                }
               
                private void Sp_DataReceived(object sender, SerialDataReceivedEventArgs e) {
                        while (sp.BytesToRead != 0) {
                                pakkeDekoder.DekodByte((byte)sp.ReadByte());
                        }
                }


                IOppsett[] oppsett;
                int tid = 0;
                private void processPackage(byte[] pakke) {
                        switch (pakke[2]) {
                                case 1:
                                        if (pakke[3] > sensors) {
                                                sensors = pakke[3];
                                                oppsett = KurveOppsett.LiveTemperaturOppsett(sensors);
                                                model.SettOpp(oppsett);
                                        }
                                        List<double>[] verdier = new List<double>[sensors + 2];
                                        for (int i = 0; i < sensors + 2; i++) {
                                                verdier[i] = new List<double>();
                                        }
                                        verdier[0].Add(tid);
                                        tid++; {
                                                double verdi = 0;
                                                double raw = BitConverter.ToUInt32(pakke, 4);
                                                verdi = 1.1 * 1023.0 * 4096.0 / raw;
                                                verdier[1].Add(verdi);
                                        }
                                        for (int i = 0; i < sensors; i++) {
                                                double verdi = 0;
                                                double raw = pakke[2 * i + 8] + (pakke[2 * i + 9] << 8);
                                                for (int j = 0; j < oppsett[i + 1].Skalering.Length; j++) {
                                                        verdi += oppsett[i + 1].Skalering[j] * Math.Pow(raw, j);
                                                }
                                                verdier[i + 2].Add(verdi);
                                        }
                                        liveFile.Write(pakke, 0, pakke.Length);
                                        liveFile.FlushAsync();
                                        model.LeggTilVerdier(verdier);
                                        break;
                        }
                }

                public bool Aktiv() {
                        return true;
                }

                public void SettModel(Model model) {
                        this.model = model;

                }

                public bool Stopp() {
                        if (liveFile != null) {
                                liveFile.Close();
                                liveFile = null;
                        }
                        if (sp.IsOpen) {

                                sp.Close();
                                return true;
                        } else {
                                return false;
                        }
                }

                public bool Start(IModel model) {
                        this.model = model;
                        if (!sp.IsOpen) {
                                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                                string logpath = Path.Combine(path, "BryggeriGUI");
                                Directory.CreateDirectory(logpath);
                                liveFile = new FileStream(Path.Combine(logpath, DateTime.Now.ToString("yyyy-MM-dd_HHmm.live")), FileMode.Create);
                                sp.Open();
                                return true;
                        } else {
                                return false;
                        }
                }

                public bool ErStartet() {
                        return sp.IsOpen;
                }
        }
}
