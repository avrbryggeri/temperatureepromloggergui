﻿using dekodlogg.interfaces;
using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace dekodlogg.live
{
    class PODBikeLive : ILive
    {
        private IModel model;
        private bool done = true;
        private System.Timers.Timer timer;
        double time = 0;
        SerialPort rpmserial;
        SerialPort dreieserial;
        double[] verdier;
        Random rnd = new Random();
        Thread rpmthread;

        public PODBikeLive(string RPMSerial, string DreieSerial)
        {
            verdier = new double[6] { 0, 0, 0, 0, 0, 0 };

            try
            {
                rpmserial = new SerialPort(RPMSerial, 19200, Parity.None, 8, StopBits.One);
                rpmserial.Open();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            if (false)
            {
                try
                {
                    dreieserial = new SerialPort(DreieSerial, 9600, Parity.None, 8, StopBits.One);
                    dreieserial.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        private void Rpmserial_Monitor() {
            SerialPort sp = rpmserial;
            while (!done) {
                string line;
                do
                {
                    line = sp.ReadLine();
                } while (line.Length < 2);
                line = line.Replace("\r", "");
                line = line.Replace("\n", "");
                Console.WriteLine("line=" + line);
                try
                {
                    if (line.Contains("C"))
                    {
                        string voltage = line.Substring(10, 3);
                        string temperature = line.Substring(1, 6);
                        Console.WriteLine("Voltage=" + voltage + ", temperature=" + temperature);
                        verdier[1] = double.Parse(temperature, CultureInfo.InvariantCulture);
                        verdier[5] = double.Parse(voltage, CultureInfo.InvariantCulture);
                    }
                    else if (line.Length > 1)
                    {
                        string rpm = line.Substring(0, 6);
                        Console.WriteLine("rpm=" + rpm);
                        verdier[0] = double.Parse(rpm, CultureInfo.InvariantCulture);
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public bool ErStartet()
        {
            return !done;
        }

        public bool Start(IModel model)
        {
            this.model = model;
            model.SettOpp(KurveOppsett.PODBikeTestOppsett());
            done = false;
            timer = new System.Timers.Timer(1000);
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
            rpmthread = new Thread(new ThreadStart(Rpmserial_Monitor));
            rpmthread.Start();
            return true;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<double>[] vals = new List<double>[7];
            for (int i = 0; i < 7; i++) vals[i] = new List<double>();
            vals[0].Add(time);
            vals[1].Add(verdier[0]);
            vals[2].Add(verdier[1]);
            vals[3].Add(verdier[2]);
            vals[4].Add(verdier[3]);
            vals[5].Add(verdier[4]);
            vals[6].Add(verdier[5]);

            model.LeggTilVerdier(vals);
            time++;
        }

        public bool Stopp()
        {
            timer.Stop();
            done = true;
            return true;
        }
    }
}
