﻿using dekodlogg.interfaces;
using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dekodlogg.live
{
    class DummyLive : IVerdiGiver
    {
        DateTime start;
        Model model;
        Timer timer;
        Random rnd;
        double temp;

        public DummyLive(double samplerate) {
            timer = new Timer();
            timer.Interval = (int)(1000.0*samplerate);
            timer.Tick += Timer_Tick;
            temp = 23;
            rnd = new Random();
        }

        public void SettModel(Model model) {
            this.model = model;
            model.SettOpp(KurveOppsett.TemperaturOppsett());
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            List<double>[] verdier = new List<double>[2];
            verdier[0] = new List<double>();
            verdier[1] = new List<double>();

            temp += (rnd.NextDouble() - 0.5)/10;
            verdier[0].Add((DateTime.Now - start).TotalSeconds);
            verdier[1].Add(temp);
            model.LeggTilVerdier(verdier);
        }

        public bool Aktiv()
        {
            return timer.Enabled;
        }

        public bool Start()
        {
            if (timer.Enabled == false)
            {
                start = DateTime.Now;
                timer.Enabled = true;
                return true;
            }
            else {
                return false;
            }
            
        }

        public bool Stopp()
        {
            if (timer.Enabled == true)
            {
                timer.Enabled = false;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
