﻿using dekodlogg.interfaces;
using dekodlogg.prosessering;
using dekodlogg.trykktemp;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dekodlogg
{
    class TrykkTempLive : ILive
    {
        IModel model;
        private string cOMPort;
        private int baudRate;
        SerialPort sp;
        PackageDecoder pd;
        DateTime tstart;
        Thread thread;
        bool done = false;
        public TrykkTempLive(string cOMPort, int baudRate)
        {
            this.cOMPort = cOMPort;
            this.baudRate = baudRate;
            pd = new PackageDecoder();
        }

        public bool ErStartet()
        {
            throw new NotImplementedException();
        }


        void Setup() {
            var oppsett = new Oppsett[6];

            oppsett[0] = new Oppsett("Trykk 1", "psi", new Farge(64, 64, 255), null, 0);
            oppsett[1] = new Oppsett("Trykk 2", "psi", new Farge(64, 64, 192), null, 0);
            oppsett[2] = new Oppsett("Trykk 3", "psi", new Farge(64, 64, 128), null, 0);

            oppsett[3] = new Oppsett("Temperatur 1", "C", new Farge(255, 64, 64), null, 0, true);
            oppsett[4] = new Oppsett("Temperatur 2", "C", new Farge(192, 64, 64), null, 0, true);
            oppsett[5] = new Oppsett("Temperatur 3", "C", new Farge(128, 64, 64), null, 0, true);

            model.SettOpp(oppsett);

            sp = new SerialPort(cOMPort, baudRate, Parity.None, 8, StopBits.One);
            //sp.DataReceived += Sp_DataReceived;
            sp.ReadTimeout = -1;
            sp.Open();
            tstart = DateTime.Now;
            thread = new Thread(new ThreadStart(CommThread));
            thread.Start();
        }

        void CommThread() {
            while(!done) { 
                byte b = (byte)sp.ReadByte();
                if (pd.ProcessByte(b)) {
                    HandlePackage(pd.GetPackage());
                }
            }
        }


      /*  private void Sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            while (sp.BytesToRead > 0) {
                if (pd.ProcessByte((byte)sp.ReadByte())) {
                    HandlePackage(pd.GetPackage());
                }
            }
        }*/

        private void HandlePackage(Package package)
        {
            switch (package.Type) {
                case 0:
                    foreach (var b in package.Payload) {
                        Console.Write((char)b);
                    }
                    break;
                case 1:
                    {
                        int averages = (int)(BitConverter.ToUInt32(package.Payload, 0));
                        var temperature = new double[5];
                        var pressure = new double[5];

                        for (int i = 0; i < 5; i++) {
                            int temp_raw = BitConverter.ToUInt16(package.Payload, 24 + 2 * i);
                            uint press_raw = BitConverter.ToUInt32(package.Payload, 0 + 4 * i);

                            temperature[i] = temp_raw == 0xFFFF ? double.NaN : temp_raw / 16.0;
                            pressure[i] = press_raw == 0xFFFFFFFF ? double.NaN : 5.0 * press_raw / averages / 1023.0;
                        }
                        double ts = (DateTime.Now - tstart).TotalSeconds;
                        var values = new List<double>[7];
                        values[0] = new List<double>() { ts };
                        
                        values[1] = new List<double>() { pressure[0] };
                        values[2] = new List<double>() { pressure[1] };
                        values[3] = new List<double>() { pressure[2] };
                        
                        values[4] = new List<double>() { temperature[0] };
                        values[5] = new List<double>() { temperature[1] };
                        values[6] = new List<double>() { temperature[2] };

                        model.LeggTilVerdier(values);

                    }
                    break;
            }
        }

        public bool Start(IModel model)
        {
            this.model = model;
            Setup();
            
            //model.LeggTilVerdier(null);
            return true;
        }

        public bool Stopp()
        {
            done = true;
            sp.Close();
            thread.Join();
            return true;
        }
    }
}
