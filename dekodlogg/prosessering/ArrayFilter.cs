﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class ArrayFilter : IDytteFilter
    {
        private double[] array;

        public ArrayFilter() {
            array = null;
        }
        public double[] Filtrer(double[] inn)
        {
            array = inn;
            return inn;
        }

        public void Nullstill()
        {
            array = null;
        }

        public double[] ToArray() {
            return array;
        }
    }
}
