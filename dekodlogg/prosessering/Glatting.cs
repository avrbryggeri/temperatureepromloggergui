﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class Glatting : IDytteFilter
    {
        private readonly IDytteFilter sluk;
        private readonly int orden;
        private double[] ringbuffer = null;
        private double akkumulator;
        private int teller;

        public Glatting(IDytteFilter sluk, int orden) {
            this.sluk = sluk;
            this.orden = orden;
            
        }

        public double[] Filtrer(double[] inn)
        {
            if (inn == null) return null;
            double[] ut = new double[inn.Length];
            if (ringbuffer == null) {
                this.ringbuffer = new double[orden];
                if (inn.Length > 0)
                {
                    this.akkumulator = inn[0]*orden;
                    for (int i = 0; i < orden; i++) {
                        this.ringbuffer[i] = inn[0];
                    }
                }
                else {
                    akkumulator = 0;
                }
                this.teller = 0;
            }

            for (int i = 0; i < inn.Length; i++) {
                akkumulator -= ringbuffer[teller];
                ringbuffer[teller] = inn[i];
                akkumulator += ringbuffer[teller];

                teller++;
                if (teller >= orden) teller = 0;
                
                ut[i] = akkumulator/orden;
            }
            if (sluk != null)
            {
                return sluk.Filtrer(ut);
            }
            else {
                return ut;
            }
        }

        public void Nullstill()
        {
            ringbuffer = null;
        }
    }
}
