﻿using System;
using System.Collections.Generic;
using dekodlogg.prosessering;
using System.IO;

namespace dekodlogg.interfaces {
        public class LiveFileDekoder : IDekoder {
                private double dt;
                private double tid = 0;
                PakkeDekoder pd;
                public IOppsett[] oppsett = null;
                List<double>[] verdier = null;

                public LiveFileDekoder(string filename, double dt) {
                        this.dt = dt;
                        byte[] log = File.ReadAllBytes(filename);
                        pd = new PakkeDekoder(PakkeMottatt);
                        for (int i = 0; i < log.Length; i++) {
                                pd.DekodByte(log[i]);
                        }
                }

                public void PakkeMottatt(byte[] pakke) {
                        int sensors = pakke[3];
                        if (oppsett == null) {
                                oppsett = KurveOppsett.LiveTemperaturOppsett(sensors);
                                verdier = new List<double>[sensors + 2];
                                for (int i = 0; i < sensors + 2; i++) {
                                        verdier[i] = new List<double>();
                                }
                        }
                       
                        verdier[0].Add(tid);
                        tid += dt;
                        {
                                double verdi = 0;
                                double raw = BitConverter.ToUInt32(pakke, 4);
                                verdi = 1.1 * 1023.0 * 4096.0 / raw;
                                verdier[1].Add(verdi);
                        }
                        for (int i = 0; i < sensors; i++) {
                                double verdi = 0;
                                double raw = pakke[2 * i + 8] + (pakke[2 * i + 9] << 8);
                                for (int j = 0; j < oppsett[i + 1].Skalering.Length; j++) {
                                        verdi += oppsett[i + 1].Skalering[j] * Math.Pow(raw, j);
                                }
                                verdier[i + 2].Add(verdi);
                        }
                }

                public IOppsett[] FaaOppsett() {
                        return oppsett;
                }

                public List<double>[] FaaVerdier() {
                        return verdier;
                }
        }
}