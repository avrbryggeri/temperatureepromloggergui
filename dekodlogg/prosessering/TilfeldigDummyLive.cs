﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class TilfeldigDummyLive : ILive
    {
        bool erStartet;
        Thread thread;
        IModel model; 

        public TilfeldigDummyLive() {
            erStartet = false;
        }

        public bool ErStartet()
        {
            return erStartet;    
        }

        public bool Start(IModel model)
        {
            if (erStartet) return false;
            this.model = model;
            thread = new Thread(new ThreadStart(LiveLoop));
            thread.Name = "Tilfeldig Dummy Live";
            erStartet = true;
            thread.Start();
            return true;
        }

        private void LiveLoop()
        {

            double tid = -12;
            double temp = 23;
            Random rnd = new Random();

            IOppsett[] oppsett = new Oppsett[1];
            oppsett[0] = new Oppsett("Temperatur", "C", null, null, 0);

            model.SettOpp(oppsett);
            List<double>[] verdier = new List<double>[2];

            while (erStartet) {

                if (tid >= 0)
                {
                    verdier[0] = new List<double>();
                    verdier[1] = new List<double>();
                    verdier[0].Add(tid);
                    verdier[1].Add(temp);
                    model.LeggTilVerdier(verdier);
                    Thread.Sleep(1000);
                }
                temp += rnd.NextDouble() - 0.5;
                tid++;
            }
        }

        public bool Stopp()
        {
            if (erStartet == false) return false;
            erStartet = false;
            thread.Join();
            return true;
        }
    }
}
