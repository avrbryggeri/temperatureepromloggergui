﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering {
        class Desimator : IKombulator {
                IKombulator sluk;
                int orden;

                IDytteFilter[] filter2 = null;
                //IDytteFilter[] filter1 = null;
                //private double dt;

                public Desimator(IKombulator sluk, int orden) {
                        this.sluk = sluk;
                        this.orden = orden;
                     //   this.dt = dt;
                }
                public List<double>[] Prosseser(List<double>[] verdier) {
                        List<double>[] nye = new List<double>[verdier.Length * 2 - 1];
                        nye[0] = new List<double>();
                        bool first = (filter2 == null);

                        if (first) {

                                filter2 = new IDytteFilter[verdier.Length - 1];
                                //filter1 = new IDytteFilter[verdier.Length - 1];
                                for (int i = 0; i < verdier.Length - 1; i++) {
                                        filter2[i] = new Glatting(null, orden);
                                        //filter1[i] = new Glatting(filter2[i], orden);
                                }
                        }

                        nye[0].AddRange(verdier[0]);
                        for (int i = 1; i < verdier.Length; i++) {
                                nye[i] = new List<double>();
                                /* if (first)
                                 {
                                     for (int j = 0; j < orden; j++)
                                     {
                                         nye[i].Add(verdier[i][0]);
                                     }
                                 }*/
                                nye[i].AddRange(verdier[i]);
                                nye[i + verdier.Length - 1] = new List<double>();
                                /* if (first) {
                                     for (int j = 0; j < orden; j++) {
                                         nye[i + verdier.Length - 1].Add(filter1[i - 1].Filtrer(new double[] { verdier[i][verdier[i].Count - 1] })[0]);
                                     }
                                 }*/
                                nye[i + verdier.Length - 1].AddRange(filter2[i - 1].Filtrer(verdier[i].ToArray()));
                                /*var last = new double[orden];
                                double siste = verdier[i][verdier[i].Count - 1];
                                for (int j = 0; j < last.Length; j++) {
                                    last[j] = siste;
                                }*/
                                //nye[i + verdier.Length - 1].AddRange(filter1[i - 1].Filtrer(last));
                                /*for (int j = 0; j < orden * 2; j++)
                                {
                                    nye[i].Add(verdier[i][0]);
                                }*/
                        }

                        if (sluk != null) {
                                return sluk.Prosseser(nye);
                        } else {
                                return nye;
                        }
                }

                public IOppsett[] SettOpp(IOppsett[] oppsett) {
                        IOppsett[] nytt = new Oppsett[oppsett.Length * 2];
                        this.filter2 = null;
                        //this.filter1 = null;
                        for (int i = 0; i < oppsett.Length; i++) {
                                nytt[i] = oppsett[i];
                                Farge nyfarge = null;
                                if (oppsett[i].Farge != null) {
                                        nyfarge = new Farge((byte)(oppsett[i].Farge.R / 2), (byte)(oppsett[i].Farge.G / 2), (byte)(oppsett[i].Farge.B / 2));
                                } else {
                                        nyfarge = null;
                                }
                                nytt[oppsett.Length + i] = new Oppsett(oppsett[i].Navn + " glattet", oppsett[i].Enhet, nyfarge, oppsett[i].Skalering, oppsett[i].Forsinkelse, false );

                        }
                        if (sluk != null) {
                                return sluk.SettOpp(nytt);
                        } else {
                                return nytt;
                        }
                }
        }
}
