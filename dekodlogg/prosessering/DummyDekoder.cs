﻿using System;
using dekodlogg.prosessering;
using System.Collections.Generic;

namespace dekodlogg.interfaces
{
    public class DummyDekoder : IDekoder
    {
        private int v;
        private double dt;

        public DummyDekoder(int v, double dt)
        {
            this.v = v;
            this.dt = dt;
        }

        public List<double>[] FaaVerdier()
        {
            Random rnd = new Random();

            double time = 0;
            double temp = 20;
            List<double>[] verdier = new List<double>[2];


            verdier[0] = new List<double>();
            verdier[1] = new List<double>();

            for (int i = 0; i < v; i++)
            {
                time += dt;
                temp += rnd.NextDouble() - 0.5;
                verdier[0].Add(time);
                verdier[1].Add(temp);
            }
            return verdier;
        }

        public IOppsett[] FaaOppsett()
        {
            return KurveOppsett.TemperaturOppsett();
        }
    }
}