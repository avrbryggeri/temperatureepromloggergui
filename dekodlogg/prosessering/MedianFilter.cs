﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class MedianFilter : IDytteFilter
    {
        private IDytteFilter sluk;
        private int orden;
        private double[] ringbuffer;
        private int counter;

        public MedianFilter(IDytteFilter sluk, int orden)
        {
            this.sluk = sluk;
            this.orden = orden;
        }

        public double[] Filtrer(double[] inn)
        {
            if (inn == null) return null;
            double[] ut = new double[inn.Length];
            if (ringbuffer == null)
            {
                this.ringbuffer = new double[orden];
                this.counter = 0;
                if (inn.Length >= 1)
                {
                    for (int i = 0; i < ringbuffer.Length; i++)
                    {
                        ringbuffer[i] = inn[0];
                    }
                }
            }
            for (int i = 0; i < inn.Length; i++)
            {
                ringbuffer[counter] = inn[i];
                double[] kopi = (double[])ringbuffer.Clone();
                Array.Sort(kopi);
                ut[i] = kopi[kopi.Length / 2];
            }
            return sluk.Filtrer(ut);
        }

        public void Nullstill()
        {
            ringbuffer = null;
        }
    }
}
