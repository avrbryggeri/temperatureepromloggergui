﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces {
        class FildekoderFabrikk {
                public static IDekoder DekodFil(string filename, double dt, char separator) {
                        string ext = Path.GetExtension(filename).ToLower();
                        if (ext == ".ebin") {
                                return new EEPROMFilDekoder(filename, dt, 0);
                        } else if (ext == ".ehbin") {
                                return new EEPROMFilDekoder(filename, dt, 1);
                        } else if (ext == ".tsv") {
                                return new TSVLeser(filename, dt, separator);
                        } else if (filename == "dummy:") {
                                return new DummyDekoder(100, dt);
                        } else if (Path.GetFileName(filename) == "TEST.TXT") {
                                return new SDCardDekoder(filename, dt);
                        } else if (ext == ".live") {
                                return new LiveFileDekoder(filename, dt);
                        } else {
                                return null;
                        }
                }
        }
}
