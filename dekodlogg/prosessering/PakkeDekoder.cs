﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering {

        public delegate void PakkeMottatt(byte[] package);

        class PakkeDekoder {
                int state = 0;
                int counter = 0;
                int sjekksum;
                byte[] pakke;
                PakkeMottatt PakkeMottatt;

                public PakkeDekoder(PakkeMottatt pakkeMottatt) {
                        this.PakkeMottatt = pakkeMottatt;
                }
                public void DekodByte(byte b) {

                        switch (state) {
                                case 0:
                                        if (b == 0xAA) {
                                                state = 1;
                                        }
                                        sjekksum = b;
                                        break;
                                case 1:
                                        sjekksum += b;
                                        pakke = new byte[3 + b];
                                        pakke[0] = 0xAA;
                                        pakke[1] = b;
                                        counter = 2;
                                        state = 2;
                                        break;
                                case 2:
                                        sjekksum += b;
                                        pakke[counter] = b;
                                        counter++;
                                        if (counter == pakke[1] + 3) {
                                                while (sjekksum > 255) {
                                                        sjekksum = (sjekksum >> 8) + (sjekksum & 0xFF);
                                                }
                                                if (((~sjekksum) & 0xFF) == 0) {
                                                        PakkeMottatt(pakke);
                                                }
                                                state = 0;
                                        }
                                        break;
                        }
                }
        }
}
