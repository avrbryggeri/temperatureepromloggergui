﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dekodlogg.brukergrensesnitt {
        public partial class SerialPortForm : Form {

                public string COMPort { get { return SerialPortCombo.Text; } }
                public int BaudRate {
                        get {
                                int ret = 9600;
                                bool success = int.TryParse(BaudRateCombo.Text, out ret);
                                if (success) return ret;
                                else return 9600;
                        }
                }
                public SerialPortForm() {
                        InitializeComponent();
                }

                private void OKButton_Click(object sender, EventArgs e) {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                }

                private void CancelButton_Click(object sender, EventArgs e) {
                        this.DialogResult = DialogResult.Cancel;
                        this.Close();
                }

                private void SerialPortCombo_DropDown(object sender, EventArgs e) {
                        SerialPortCombo.Items.Clear();
                        SerialPortCombo.Items.AddRange(SerialPort.GetPortNames());
                }
        }
}
