﻿namespace dekodlogg.brukergrensesnitt
{
    partial class PODBikeTestSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.DreiemomentCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LukkButton = new System.Windows.Forms.Button();
            this.StartButton = new System.Windows.Forms.Button();
            this.RPMCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AnnenCombo = new System.Windows.Forms.ComboBox();
            this.KalibreringsButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.DreiemomentCombo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.LukkButton, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.StartButton, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.RPMCombo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.AnnenCombo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.KalibreringsButton, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(648, 279);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "... serieport";
            // 
            // DreiemomentCombo
            // 
            this.DreiemomentCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DreiemomentCombo.FormattingEnabled = true;
            this.DreiemomentCombo.Location = new System.Drawing.Point(327, 55);
            this.DreiemomentCombo.Name = "DreiemomentCombo";
            this.DreiemomentCombo.Size = new System.Drawing.Size(318, 28);
            this.DreiemomentCombo.TabIndex = 5;
            this.DreiemomentCombo.DropDown += new System.EventHandler(this.DreiemomentCombo_DropDown);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Dreiemoment serieport";
            // 
            // LukkButton
            // 
            this.LukkButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LukkButton.Location = new System.Drawing.Point(107, 239);
            this.LukkButton.Name = "LukkButton";
            this.LukkButton.Size = new System.Drawing.Size(109, 30);
            this.LukkButton.TabIndex = 0;
            this.LukkButton.Text = "Lukk";
            this.LukkButton.UseVisualStyleBackColor = true;
            this.LukkButton.Click += new System.EventHandler(this.LukkButton_Click);
            // 
            // StartButton
            // 
            this.StartButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StartButton.Location = new System.Drawing.Point(431, 239);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(109, 30);
            this.StartButton.TabIndex = 1;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // RPMCombo
            // 
            this.RPMCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.RPMCombo.FormattingEnabled = true;
            this.RPMCombo.Location = new System.Drawing.Point(327, 9);
            this.RPMCombo.Name = "RPMCombo";
            this.RPMCombo.Size = new System.Drawing.Size(318, 28);
            this.RPMCombo.TabIndex = 2;
            this.RPMCombo.DropDown += new System.EventHandler(this.RPMCombo_DropDown);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "RPM serieport";
            // 
            // AnnenCombo
            // 
            this.AnnenCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.AnnenCombo.FormattingEnabled = true;
            this.AnnenCombo.Location = new System.Drawing.Point(327, 101);
            this.AnnenCombo.Name = "AnnenCombo";
            this.AnnenCombo.Size = new System.Drawing.Size(318, 28);
            this.AnnenCombo.TabIndex = 6;
            // 
            // KalibreringsButton
            // 
            this.KalibreringsButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.KalibreringsButton.Location = new System.Drawing.Point(78, 187);
            this.KalibreringsButton.Name = "KalibreringsButton";
            this.KalibreringsButton.Size = new System.Drawing.Size(167, 39);
            this.KalibreringsButton.TabIndex = 8;
            this.KalibreringsButton.Text = "Kalibrering fra fil...";
            this.KalibreringsButton.UseVisualStyleBackColor = true;
            // 
            // PODBikeTestSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 279);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PODBikeTestSetupForm";
            this.Text = "PODBike Test Setup";
            this.Load += new System.EventHandler(this.PODBikeTestSetupForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox DreiemomentCombo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button LukkButton;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.ComboBox RPMCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox AnnenCombo;
        private System.Windows.Forms.Button KalibreringsButton;
    }
}