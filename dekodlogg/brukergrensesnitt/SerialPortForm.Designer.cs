﻿namespace dekodlogg.brukergrensesnitt {
        partial class SerialPortForm {
                /// <summary>
                /// Required designer variable.
                /// </summary>
                private System.ComponentModel.IContainer components = null;

                /// <summary>
                /// Clean up any resources being used.
                /// </summary>
                /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
                protected override void Dispose(bool disposing) {
                        if (disposing && (components != null)) {
                                components.Dispose();
                        }
                        base.Dispose(disposing);
                }

                #region Windows Form Designer generated code

                /// <summary>
                /// Required method for Designer support - do not modify
                /// the contents of this method with the code editor.
                /// </summary>
                private void InitializeComponent() {
                        this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
                        this.CancelButton2 = new System.Windows.Forms.Button();
                        this.OKButton = new System.Windows.Forms.Button();
                        this.label1 = new System.Windows.Forms.Label();
                        this.label3 = new System.Windows.Forms.Label();
                        this.SerialPortCombo = new System.Windows.Forms.ComboBox();
                        this.BaudRateCombo = new System.Windows.Forms.ComboBox();
                        this.tableLayoutPanel1.SuspendLayout();
                        this.SuspendLayout();
                        // 
                        // tableLayoutPanel1
                        // 
                        this.tableLayoutPanel1.ColumnCount = 3;
                        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
                        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
                        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
                        this.tableLayoutPanel1.Controls.Add(this.BaudRateCombo, 1, 1);
                        this.tableLayoutPanel1.Controls.Add(this.CancelButton2, 0, 3);
                        this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
                        this.tableLayoutPanel1.Controls.Add(this.OKButton, 2, 3);
                        this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
                        this.tableLayoutPanel1.Controls.Add(this.SerialPortCombo, 1, 0);
                        this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
                        this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
                        this.tableLayoutPanel1.Name = "tableLayoutPanel1";
                        this.tableLayoutPanel1.RowCount = 4;
                        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
                        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
                        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
                        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
                        this.tableLayoutPanel1.Size = new System.Drawing.Size(367, 124);
                        this.tableLayoutPanel1.TabIndex = 0;
                        // 
                        // CancelButton
                        // 
                        this.CancelButton2.Anchor = System.Windows.Forms.AnchorStyles.None;
                        this.CancelButton2.Location = new System.Drawing.Point(23, 97);
                        this.CancelButton2.Name = "CancelButton";
                        this.CancelButton2.Size = new System.Drawing.Size(75, 23);
                        this.CancelButton2.TabIndex = 0;
                        this.CancelButton2.Text = "Avbryt";
                        this.CancelButton2.UseVisualStyleBackColor = true;
                        this.CancelButton2.Click += new System.EventHandler(this.CancelButton_Click);
                        // 
                        // OKButton
                        // 
                        this.OKButton.Anchor = System.Windows.Forms.AnchorStyles.None;
                        this.OKButton.Location = new System.Drawing.Point(268, 97);
                        this.OKButton.Name = "OKButton";
                        this.OKButton.Size = new System.Drawing.Size(75, 23);
                        this.OKButton.TabIndex = 1;
                        this.OKButton.Text = "OK";
                        this.OKButton.UseVisualStyleBackColor = true;
                        this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
                        // 
                        // label1
                        // 
                        this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
                        this.label1.AutoSize = true;
                        this.label1.Location = new System.Drawing.Point(3, 9);
                        this.label1.Name = "label1";
                        this.label1.Size = new System.Drawing.Size(49, 13);
                        this.label1.TabIndex = 2;
                        this.label1.Text = "Serieport";
                        // 
                        // label3
                        // 
                        this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
                        this.label3.AutoSize = true;
                        this.label3.Location = new System.Drawing.Point(3, 40);
                        this.label3.Name = "label3";
                        this.label3.Size = new System.Drawing.Size(53, 13);
                        this.label3.TabIndex = 3;
                        this.label3.Text = "Baud rate";
                        // 
                        // SerialPortCombo
                        // 
                        this.SerialPortCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
                        this.tableLayoutPanel1.SetColumnSpan(this.SerialPortCombo, 2);
                        this.SerialPortCombo.FormattingEnabled = true;
                        this.SerialPortCombo.Location = new System.Drawing.Point(125, 5);
                        this.SerialPortCombo.Name = "SerialPortCombo";
                        this.SerialPortCombo.Size = new System.Drawing.Size(239, 21);
                        this.SerialPortCombo.TabIndex = 4;
                        this.SerialPortCombo.DropDown += new System.EventHandler(this.SerialPortCombo_DropDown);
                        // 
                        // BaudRateCombo
                        // 
                        this.BaudRateCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
                        this.tableLayoutPanel1.SetColumnSpan(this.BaudRateCombo, 2);
                        this.BaudRateCombo.FormattingEnabled = true;
                        this.BaudRateCombo.Items.AddRange(new object[] {
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200",
            "230400"});
                        this.BaudRateCombo.Location = new System.Drawing.Point(125, 36);
                        this.BaudRateCombo.Name = "BaudRateCombo";
                        this.BaudRateCombo.Size = new System.Drawing.Size(239, 21);
                        this.BaudRateCombo.TabIndex = 5;
                        this.BaudRateCombo.Text = "9600";
                        // 
                        // SerialPortForm
                        // 
                        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                        this.ClientSize = new System.Drawing.Size(367, 124);
                        this.Controls.Add(this.tableLayoutPanel1);
                        this.Name = "SerialPortForm";
                        this.Text = "Velg serieport";
                        this.tableLayoutPanel1.ResumeLayout(false);
                        this.tableLayoutPanel1.PerformLayout();
                        this.ResumeLayout(false);

                }

                #endregion

                private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
                private System.Windows.Forms.Button CancelButton2;
                private System.Windows.Forms.Label label1;
                private System.Windows.Forms.Button OKButton;
                private System.Windows.Forms.ComboBox BaudRateCombo;
                private System.Windows.Forms.Label label3;
                private System.Windows.Forms.ComboBox SerialPortCombo;
        }
}