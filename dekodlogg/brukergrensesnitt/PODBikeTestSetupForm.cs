﻿using dekodlogg.io;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dekodlogg.brukergrensesnitt
{
    public partial class PODBikeTestSetupForm : Form
    {
        public PODBikeTestSetupForm()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            ConfigFile.Inst().Set("RPM serial", RPMCombo.Text);
            ConfigFile.Inst().Set("Dreiemoment serial", DreiemomentCombo.Text);
            this.DialogResult = DialogResult.OK;
        }

        private void LukkButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void PODBikeTestSetupForm_Load(object sender, EventArgs e)
        {
            RPMCombo.Text = ConfigFile.Inst().Get("RPM serial");
            DreiemomentCombo.Text = ConfigFile.Inst().Get("Dreiemoment serial");
        }

        private void RPMCombo_DropDown(object sender, EventArgs e)
        {
            RPMCombo.Items.Clear();
            RPMCombo.Items.AddRange(SerialPort.GetPortNames());
        }

        private void DreiemomentCombo_DropDown(object sender, EventArgs e)
        {
            DreiemomentCombo.Items.Clear();
            DreiemomentCombo.Items.AddRange(SerialPort.GetPortNames());
        }

        public String GetRPMSerial() {
            return RPMCombo.Text;
        }
        public String GetDreiemomentSerial()
        {
            return DreiemomentCombo.Text;
        }
    }
}
