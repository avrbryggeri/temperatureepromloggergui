﻿namespace dekodlogg
{
    partial class GUIPlot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lagreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eksporterTilPNGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eksporterTilPNGBokformatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kobleTilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serieportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pODBikeTestOppsettToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dummyPlotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stoppLiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lagFFFFFilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hjelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.omToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.autoSCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.LagreTilCSVCheckBox = new System.Windows.Forms.CheckBox();
            this.grafCheckbox = new System.Windows.Forms.CheckedListBox();
            this.glatteOrdenNumeric = new System.Windows.Forms.NumericUpDown();
            this.GlattingCheckbox = new System.Windows.Forms.CheckBox();
            this.sampleTidNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.trykkTempToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.glatteOrdenNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sampleTidNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filToolStripMenuItem,
            this.testToolStripMenuItem,
            this.annetToolStripMenuItem,
            this.hjelpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(948, 35);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filToolStripMenuItem
            // 
            this.filToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.lagreToolStripMenuItem,
            this.eksporterTilPNGToolStripMenuItem,
            this.eksporterTilPNGBokformatToolStripMenuItem,
            this.kobleTilToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.filToolStripMenuItem.Name = "filToolStripMenuItem";
            this.filToolStripMenuItem.Size = new System.Drawing.Size(45, 29);
            this.filToolStripMenuItem.Text = "Fil";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.openToolStripMenuItem.Text = "Åpne logg...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // lagreToolStripMenuItem
            // 
            this.lagreToolStripMenuItem.Name = "lagreToolStripMenuItem";
            this.lagreToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.lagreToolStripMenuItem.Text = "Lagre som...";
            this.lagreToolStripMenuItem.Click += new System.EventHandler(this.lagreToolStripMenuItem_Click);
            // 
            // eksporterTilPNGToolStripMenuItem
            // 
            this.eksporterTilPNGToolStripMenuItem.Name = "eksporterTilPNGToolStripMenuItem";
            this.eksporterTilPNGToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.eksporterTilPNGToolStripMenuItem.Text = "Eksporter til .PNG";
            this.eksporterTilPNGToolStripMenuItem.Click += new System.EventHandler(this.eksporterTilPNGToolStripMenuItem_Click);
            // 
            // eksporterTilPNGBokformatToolStripMenuItem
            // 
            this.eksporterTilPNGBokformatToolStripMenuItem.Name = "eksporterTilPNGBokformatToolStripMenuItem";
            this.eksporterTilPNGBokformatToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.eksporterTilPNGBokformatToolStripMenuItem.Text = "Eksporter til .PNG for bok";
            this.eksporterTilPNGBokformatToolStripMenuItem.Click += new System.EventHandler(this.eksporterTilPNGBokformatToolStripMenuItem_Click);
            // 
            // kobleTilToolStripMenuItem
            // 
            this.kobleTilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serieportToolStripMenuItem,
            this.pODBikeTestOppsettToolStripMenuItem});
            this.kobleTilToolStripMenuItem.Name = "kobleTilToolStripMenuItem";
            this.kobleTilToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.kobleTilToolStripMenuItem.Text = "Koble til";
            // 
            // serieportToolStripMenuItem
            // 
            this.serieportToolStripMenuItem.Name = "serieportToolStripMenuItem";
            this.serieportToolStripMenuItem.Size = new System.Drawing.Size(296, 34);
            this.serieportToolStripMenuItem.Text = "Serieport...";
            this.serieportToolStripMenuItem.Click += new System.EventHandler(this.serieportToolStripMenuItem_Click);
            // 
            // pODBikeTestOppsettToolStripMenuItem
            // 
            this.pODBikeTestOppsettToolStripMenuItem.Name = "pODBikeTestOppsettToolStripMenuItem";
            this.pODBikeTestOppsettToolStripMenuItem.Size = new System.Drawing.Size(296, 34);
            this.pODBikeTestOppsettToolStripMenuItem.Text = "PODBike test oppsett...";
            this.pODBikeTestOppsettToolStripMenuItem.Click += new System.EventHandler(this.pODBikeTestOppsettToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(316, 34);
            this.exitToolStripMenuItem.Text = "Lukk program";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trykkTempToolStripMenuItem,
            this.dummyPlotToolStripMenuItem,
            this.liveToolStripMenuItem,
            this.stoppLiveToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(58, 29);
            this.testToolStripMenuItem.Text = "Test";
            // 
            // dummyPlotToolStripMenuItem
            // 
            this.dummyPlotToolStripMenuItem.Name = "dummyPlotToolStripMenuItem";
            this.dummyPlotToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.dummyPlotToolStripMenuItem.Text = "Dummy plot";
            this.dummyPlotToolStripMenuItem.Click += new System.EventHandler(this.dummyPlotToolStripMenuItem_Click);
            // 
            // liveToolStripMenuItem
            // 
            this.liveToolStripMenuItem.Name = "liveToolStripMenuItem";
            this.liveToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.liveToolStripMenuItem.Text = "Start Live";
            this.liveToolStripMenuItem.Click += new System.EventHandler(this.liveToolStripMenuItem_Click);
            // 
            // stoppLiveToolStripMenuItem
            // 
            this.stoppLiveToolStripMenuItem.Name = "stoppLiveToolStripMenuItem";
            this.stoppLiveToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.stoppLiveToolStripMenuItem.Text = "Stopp Live";
            this.stoppLiveToolStripMenuItem.Click += new System.EventHandler(this.stoppLiveToolStripMenuItem_Click);
            // 
            // annetToolStripMenuItem
            // 
            this.annetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lagFFFFFilToolStripMenuItem});
            this.annetToolStripMenuItem.Name = "annetToolStripMenuItem";
            this.annetToolStripMenuItem.Size = new System.Drawing.Size(75, 29);
            this.annetToolStripMenuItem.Text = "Annet";
            // 
            // lagFFFFFilToolStripMenuItem
            // 
            this.lagFFFFFilToolStripMenuItem.Name = "lagFFFFFilToolStripMenuItem";
            this.lagFFFFFilToolStripMenuItem.Size = new System.Drawing.Size(214, 34);
            this.lagFFFFFilToolStripMenuItem.Text = "Lag FFFF fil...";
            this.lagFFFFFilToolStripMenuItem.Click += new System.EventHandler(this.lagFFFFFilToolStripMenuItem_Click_1);
            // 
            // hjelpToolStripMenuItem
            // 
            this.hjelpToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.hjelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.omToolStripMenuItem});
            this.hjelpToolStripMenuItem.Name = "hjelpToolStripMenuItem";
            this.hjelpToolStripMenuItem.Size = new System.Drawing.Size(69, 29);
            this.hjelpToolStripMenuItem.Text = "Hjelp";
            // 
            // omToolStripMenuItem
            // 
            this.omToolStripMenuItem.Name = "omToolStripMenuItem";
            this.omToolStripMenuItem.Size = new System.Drawing.Size(144, 34);
            this.omToolStripMenuItem.Text = "Om";
            this.omToolStripMenuItem.Click += new System.EventHandler(this.omToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 35);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(948, 662);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.autoSCheckBox);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.LagreTilCSVCheckBox);
            this.panel1.Controls.Add(this.grafCheckbox);
            this.panel1.Controls.Add(this.glatteOrdenNumeric);
            this.panel1.Controls.Add(this.GlattingCheckbox);
            this.panel1.Controls.Add(this.sampleTidNumeric);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 652);
            this.panel1.TabIndex = 3;
            // 
            // autoSCheckBox
            // 
            this.autoSCheckBox.AutoSize = true;
            this.autoSCheckBox.Location = new System.Drawing.Point(0, 572);
            this.autoSCheckBox.Name = "autoSCheckBox";
            this.autoSCheckBox.Size = new System.Drawing.Size(111, 24);
            this.autoSCheckBox.TabIndex = 10;
            this.autoSCheckBox.Text = "Autoskaler";
            this.autoSCheckBox.UseVisualStyleBackColor = true;
            this.autoSCheckBox.CheckedChanged += new System.EventHandler(this.autoSCheckBox_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(4, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 31);
            this.button1.TabIndex = 9;
            this.button1.Text = "Sett";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LagreTilCSVCheckBox
            // 
            this.LagreTilCSVCheckBox.AutoSize = true;
            this.LagreTilCSVCheckBox.Location = new System.Drawing.Point(0, 306);
            this.LagreTilCSVCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LagreTilCSVCheckBox.Name = "LagreTilCSVCheckBox";
            this.LagreTilCSVCheckBox.Size = new System.Drawing.Size(132, 24);
            this.LagreTilCSVCheckBox.TabIndex = 8;
            this.LagreTilCSVCheckBox.Text = "Lagre til .CSV";
            this.LagreTilCSVCheckBox.UseVisualStyleBackColor = true;
            this.LagreTilCSVCheckBox.CheckedChanged += new System.EventHandler(this.LagreTilCSVCheckBox_CheckedChanged);
            this.LagreTilCSVCheckBox.Click += new System.EventHandler(this.LagreTilCSVCheckBox_Click);
            // 
            // grafCheckbox
            // 
            this.grafCheckbox.CheckOnClick = true;
            this.grafCheckbox.FormattingEnabled = true;
            this.grafCheckbox.Location = new System.Drawing.Point(-2, 392);
            this.grafCheckbox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grafCheckbox.Name = "grafCheckbox";
            this.grafCheckbox.Size = new System.Drawing.Size(186, 165);
            this.grafCheckbox.TabIndex = 7;
            this.grafCheckbox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.grafCheckbox_ItemCheck);
            this.grafCheckbox.SelectedIndexChanged += new System.EventHandler(this.grafCheckbox_SelectedIndexChanged);
            // 
            // glatteOrdenNumeric
            // 
            this.glatteOrdenNumeric.Location = new System.Drawing.Point(4, 162);
            this.glatteOrdenNumeric.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.glatteOrdenNumeric.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.glatteOrdenNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.glatteOrdenNumeric.Name = "glatteOrdenNumeric";
            this.glatteOrdenNumeric.Size = new System.Drawing.Size(180, 26);
            this.glatteOrdenNumeric.TabIndex = 6;
            this.glatteOrdenNumeric.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.glatteOrdenNumeric.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // GlattingCheckbox
            // 
            this.GlattingCheckbox.AutoSize = true;
            this.GlattingCheckbox.Location = new System.Drawing.Point(4, 126);
            this.GlattingCheckbox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GlattingCheckbox.Name = "GlattingCheckbox";
            this.GlattingCheckbox.Size = new System.Drawing.Size(91, 24);
            this.GlattingCheckbox.TabIndex = 5;
            this.GlattingCheckbox.Text = "Glatting";
            this.GlattingCheckbox.UseVisualStyleBackColor = true;
            this.GlattingCheckbox.CheckedChanged += new System.EventHandler(this.GlattingCheckbox_CheckedChanged);
            // 
            // sampleTidNumeric
            // 
            this.sampleTidNumeric.Location = new System.Drawing.Point(4, 86);
            this.sampleTidNumeric.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sampleTidNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.sampleTidNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sampleTidNumeric.Name = "sampleTidNumeric";
            this.sampleTidNumeric.Size = new System.Drawing.Size(180, 26);
            this.sampleTidNumeric.TabIndex = 3;
            this.sampleTidNumeric.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sampletid [s]";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Sekund",
            "Minutt",
            "Time",
            "Dag",
            "Uke"});
            this.comboBox1.Location = new System.Drawing.Point(4, 25);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 28);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.Text = "Sekund";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tid";
            // 
            // trykkTempToolStripMenuItem
            // 
            this.trykkTempToolStripMenuItem.Name = "trykkTempToolStripMenuItem";
            this.trykkTempToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.trykkTempToolStripMenuItem.Text = "Trykk temp";
            this.trykkTempToolStripMenuItem.Click += new System.EventHandler(this.trykkTempToolStripMenuItem_Click);
            // 
            // GUIPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 697);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GUIPlot";
            this.Text = "GUIPlot";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GUIPlot_FormClosing);
            this.Load += new System.EventHandler(this.GUIPlot_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.glatteOrdenNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sampleTidNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dummyPlotToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown sampleTidNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem lagreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stoppLiveToolStripMenuItem;
        private System.Windows.Forms.CheckBox GlattingCheckbox;
        private System.Windows.Forms.ToolStripMenuItem annetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lagFFFFFilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hjelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem omToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown glatteOrdenNumeric;
        private System.Windows.Forms.ToolStripMenuItem eksporterTilPNGToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox grafCheckbox;
        private System.Windows.Forms.ToolStripMenuItem eksporterTilPNGBokformatToolStripMenuItem;
                private System.Windows.Forms.ToolStripMenuItem kobleTilToolStripMenuItem;
                private System.Windows.Forms.ToolStripMenuItem serieportToolStripMenuItem;
                private System.Windows.Forms.CheckBox LagreTilCSVCheckBox;
        private System.Windows.Forms.ToolStripMenuItem pODBikeTestOppsettToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox autoSCheckBox;
        private System.Windows.Forms.ToolStripMenuItem trykkTempToolStripMenuItem;
    }
}