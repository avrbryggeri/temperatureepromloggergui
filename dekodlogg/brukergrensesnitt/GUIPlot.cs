﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using dekodlogg.prosessering;

using dekodlogg.live;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.IO;
using OxyPlot.WindowsForms;
using dekodlogg.brukergrensesnitt;
using dekodlogg.io;

namespace dekodlogg
{
    public partial class GUIPlot : Form, IBrukerGrensesnitt
    {
        private IModel model;
        double skalering = 1;
        private OxyPlot.WindowsForms.PlotView plot1;
        private PlotModel myModel;
        private LineSeries[] series;
        ILive live;
        private IOppsett[] oppsett = null;
        TSVLiveSkriver tsvLiveSkriver = null;

        public GUIPlot()
        {
            InitializeComponent();
            plot1 = new OxyPlot.WindowsForms.PlotView();

            this.plot1.Dock = DockStyle.Fill;
            plot1.BackColor = System.Drawing.Color.White;
            myModel = new PlotModel();
            myModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Tid" });
            myModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Verdi" });

            this.plot1.Model = myModel;
            this.tableLayoutPanel1.Controls.Add(this.plot1, 1, 0);
            glatteOrdenNumeric.Enabled = GlattingCheckbox.Checked;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "EEPROM binary files(*.ebin)|*.ebin|EEPROM high res binary files(*.ehbin)|*.ehbin|Tab separated files(*.tsv)|*.tsv|Live files(*.live)|*.live|All files(*.*)|*.*";
            ofd.FilterIndex = 0;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                IDekoder vg = FildekoderFabrikk.DekodFil(ofd.FileName, (double)sampleTidNumeric.Value, '\t');

                model.SettOpp(vg.FaaOppsett());
                model.LeggTilVerdier(vg.FaaVerdier());
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dummyPlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            model.LesFraFil("dummy:", (double)sampleTidNumeric.Value);
        }

        private void OppdaterSkalering()
        {
            if (comboBox1.Text == "Sekund")
            {
                skalering = 1.0;
                // chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Seconds;
            }
            else if (comboBox1.Text == "Minutt")
            {
                skalering = 1.0 / 60.0;
                // chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Minutes;
            }
            else if (comboBox1.Text == "Time")
            {
                skalering = 1.0 / 3600.0;
                //chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Hours;
            }
            else if (comboBox1.Text == "Dag")
            {
                skalering = 1.0 / (24.0 * 3600.0);
                //chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Days;
            }
            else if (comboBox1.Text == "Uke")
            {
                skalering = 1.0 / (7.0 * 24.0 * 3600.0);
                //chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Weeks;
            }
            else
            {
                comboBox1.Text = "Time";
                skalering = 1.0 / 3600.0;
                //chart1.ChartAreas[0].Axes[0].IntervalType = DateTimeIntervalType.Hours;
            }


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            OppdaterSkalering();
            OppdaterPlott();
        }

        private void OppdaterPlott()
        {

            List<double>[] Verdier;
            Verdier = model.FaaVerdier();
            if (Verdier == null) return;
            SettOpp(model.FaaOppsett());
            PlotVerdier(Verdier);
        }

        private void lagreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Tab separated files(*.tsv)|*.tsv|All files(*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                model.LagreTilFil(sfd.FileName);
            }
        }

        public void SettOpp(IOppsett[] kurver)
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                this.oppsett = kurver;
                series = new LineSeries[kurver.Length];

                myModel = new PlotModel();
                myModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot });
                myModel.Axes.Add(new LinearAxis { Key="Primary", Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot });
                myModel.Axes.Add(new LinearAxis { Key="Secondary", Position = AxisPosition.Right });

                grafCheckbox.Items.Clear();
                for (int i = 0; i < kurver.Length; i++)
                {
                    series[i] = new LineSeries { Title = kurver[i].ToString(), StrokeThickness = 4 };
                    series[i].YAxisKey = kurver[i].SecondaryAxis ? "Secondary" : "Primary";
                    if (kurver[i].Farge != null) series[i].Color = OxyColor.FromRgb(kurver[i].Farge.R, kurver[i].Farge.G, kurver[i].Farge.B);
                    myModel.Series.Add(series[i]);
                    grafCheckbox.Items.Add(kurver[i].ToString());
                }
                for (int i = 0; i < kurver.Length; i++)
                {
                    grafCheckbox.SetItemChecked(i, true);
                }
                plot1.Model = myModel;
            });
            foreach (IOppsett oppsett in kurver)
            {
                System.Console.Write(oppsett.ToString() + "\t");
            }
            Console.WriteLine();
        }

        public void SettModel(IModel model)
        {
            this.model = model;
        }

        private void liveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (live != null) live.Stopp();
            live = new TilfeldigDummyLive();
            live.Start(model);
        }

        private void stoppLiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            live.Stopp();
        }

        private void GUIPlot_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (live != null) live.Stopp();
        }

        public void PlotVerdier(List<double>[] verdier)
        {

            this.BeginInvoke((MethodInvoker)delegate
            {
                for (int j = 1; j < verdier.Length; j++)
                {
                    for (int i = 0; i < verdier[0].Count; i++)
                    {
                        if (!double.IsNaN(verdier[j][i]))
                        {
                            series[j - 1].Points.Add(new DataPoint((verdier[0][i] - oppsett[j - 1].Forsinkelse) * skalering, verdier[j][i]));
                        }
                        else {
                            series[j - 1].Points.Add(new DataPoint((verdier[0][i] - oppsett[j - 1].Forsinkelse) * skalering, 0.0));
                        }
                    }
                    Console.Write(verdier[j][verdier[j].Count-1] + "\t");
                }
                Console.WriteLine();
                //myModel.Axes[0].Position = AxisPosition.Bottom;
                myModel.Axes[0].Title = "Tid [" + comboBox1.Text + "]";
                plot1.Refresh();
                if (tsvLiveSkriver != null) tsvLiveSkriver.LagreVerdier(verdier);
            });
        }


        private void lagFFFFFilToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            byte[] ffff = new byte[65536];
            for (int i = 0; i < ffff.Length; i++) ffff[i] = 0xFF;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "FFFF files(*.txt)|*.txt|All files(*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(sfd.FileName, ffff);
            }
        }

        private void omToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Du får finna ut kossen programme virke sjøl.");
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            model.SettKombulator(new Glatter(null, (int)glatteOrdenNumeric.Value, (double)(sampleTidNumeric.Value)));
        }

        private void GlattingCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            glatteOrdenNumeric.Enabled = GlattingCheckbox.Checked;
            if (GlattingCheckbox.Checked)
            {
                model.SettKombulator(new Glatter(null, (int)glatteOrdenNumeric.Value, (double)sampleTidNumeric.Value));
            }
            else
            {
                model.SettKombulator(null);
            }
        }
        private void eksporterPNG(int w, int h)
        {
            var pngExporter = new PngExporter { Width = w, Height = h, Background = OxyColors.White };
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PNG file(*.png)|*.png|All files(*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                pngExporter.ExportToFile(myModel, sfd.FileName);
            }
        }
        private void eksporterTilPNGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eksporterPNG(2339 / 2, 1653 / 2);
        }

        private void grafCheckbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*int index = grafCheckbox.SelectedIndex;
            myModel.Series[index].IsVisible = grafCheckbox.GetItemChecked(index);
            plot1.Refresh();*/
        }

        private void grafCheckbox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            myModel.Series[e.Index].IsVisible = (e.NewValue == CheckState.Checked);
            plot1.Refresh();
        }

        private void eksporterTilPNGBokformatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eksporterPNG(426, 160);
        }

        private void serieportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SerialPortForm spf = new SerialPortForm();
            if (spf.ShowDialog() == DialogResult.OK)
            {
                live = new SeriePortLive(spf.COMPort, spf.BaudRate);
                live.Start(model);
            }

        }

        private void LagreTilCSVCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LagreTilCSVCheckBox_Click(object sender, EventArgs e)
        {
            if (LagreTilCSVCheckBox.Checked)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Tab separerte filer(*.tsv)|*.tsv|Tekst filer(*.txt)|*.txt";
                sfd.FilterIndex = 0;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    tsvLiveSkriver = new TSVLiveSkriver(sfd.FileName, model.FaaOppsett(), comboBox1.Text, "\t");
                    tsvLiveSkriver.LagreVerdier(model.FaaVerdier());
                }
                else
                {
                    LagreTilCSVCheckBox.Checked = false;
                }
            }
            else
            {
                if (tsvLiveSkriver != null)
                {
                    tsvLiveSkriver.Close();
                    tsvLiveSkriver = null;
                }
            }
        }

        private void pODBikeTestOppsettToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PODBikeTestSetupForm pts = new PODBikeTestSetupForm();
            if (pts.ShowDialog() == DialogResult.OK)
            {
                live = new PODBikeLive(pts.GetRPMSerial(), pts.GetDreiemomentSerial());
                live.Start(model);
            }
            else
            {
                MessageBox.Show("Skal IKKE starte live ..");
            }
        }

        private void GUIPlot_Load(object sender, EventArgs e)
        {
            ConfigFile.Inst().Set("test1", "test2");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            model.SettKombulator(new Glatter(null, (int)glatteOrdenNumeric.Value, (double)(sampleTidNumeric.Value)));
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void autoSCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void trykkTempToolStripMenuItem_Click(object sender, EventArgs e)
        {
           

            SerialPortForm spf = new SerialPortForm();
            if (spf.ShowDialog() == DialogResult.OK)
            {
                var tt = new TrykkTempLive(spf.COMPort, spf.BaudRate);
                tt.Start(model);
            }
        }
    }
}
