﻿using dekodlogg.interfaces;
using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dekodlogg.live;
//using dekodlogg.model;

namespace dekodlogg
{
    public class Model : IModel
    {
        private char separator = '\t';
        private IBrukerGrensesnitt bg;

        public IOppsett[] Oppsett { get; private set; }
        private List<double>[] Verdier { get; set; }

        IKombulator kombulator = null;


        public void SettOpp(IOppsett[] oppsett)
        {
            if (oppsett == null) return;
            this.Oppsett = oppsett;
            Verdier = new List<double>[oppsett.Length + 1];
            for (int i = 0; i < oppsett.Length + 1; i++)
            {
                Verdier[i] = new List<double>();
            }

            if (kombulator == null)
            {
                bg.SettOpp(oppsett);    
            }
            else
            {
                bg.SettOpp(kombulator.SettOpp(oppsett));
            }
        }

        public Model(IBrukerGrensesnitt bg)
        {
            this.bg = bg;
            this.bg.SettModel(this);
        }

        public void LeggTilVerdier(List<double>[] verdier)
        {
            if (verdier == null) return;
            for (int i = 0; i < verdier.Length; i++)
            {
                Verdier[i].AddRange(verdier[i]);
            }
            
            if (kombulator == null)
            {
                bg.PlotVerdier(verdier);
            }
            else
            {
                bg.PlotVerdier(kombulator.Prosseser(verdier));
            }
        }

        public string FaaTidslabel()
        {
            return "Tid [sekund]";
        }

        public void LagreTilFil(string filnavn)
        {
            IFilskriver filskriver = new TSVSkriver(separator);
            filskriver.Lagre(filnavn, FaaOppsett(), FaaVerdier(), FaaTidslabel());
        }

        public void LesFraFil(string filnavn, double dt)
        {
            IDekoder vg = FildekoderFabrikk.DekodFil(filnavn, dt, separator);
            Oppsett = vg.FaaOppsett();
            Verdier = vg.FaaVerdier();
            bg.SettOpp(Oppsett);
            bg.PlotVerdier(Verdier);
        }

        public List<double>[] FaaVerdier()
        {
            if (kombulator != null)
            {
                return kombulator.Prosseser(Verdier);
            }
            else {
                return Verdier;
            }
        }

        public IOppsett[] FaaOppsett()
        {
            if (kombulator != null)
            {
                return kombulator.SettOpp(Oppsett);
            }
            else
            {
                return Oppsett;
            }
        }

        public void SettKombulator(IKombulator kombulator)
        {
            var gamleVerdier = Verdier;
            this.kombulator = kombulator;
            SettOpp(Oppsett);
            LeggTilVerdier(gamleVerdier);
        }
    }
}
