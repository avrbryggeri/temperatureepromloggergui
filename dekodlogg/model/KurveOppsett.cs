﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class KurveOppsett
    {
        public static Oppsett[] LiveTemperaturOppsett(int n)
        {
            Oppsett[] oppsett = new Oppsett[n + 1];

            oppsett[0] = new Oppsett("Spenning", "Volt", new Farge(128, 0, 64), new double[] { 0.0, 1.0 }, 0.0);

            for (int i = 0; i < n; i++)
            {
                oppsett[i + 1] = new Oppsett("Temperatur " + i, "°C", null, new double[] { 0.0, 1.0 / 16.0 }, 0);
            }
            return oppsett;
        }

        public static Oppsett[] TemperaturOppsett()
        {
            Oppsett[] oppsett = new Oppsett[1];
            oppsett[0] = new Oppsett("Temperatur", "°C", new Farge(255, 0, 0), null, 0);
            return oppsett;
        }

        public static Oppsett[] PODBikeTestOppsett()
        {
            Oppsett[] oppsett = new Oppsett[6];
            oppsett[0] = new Oppsett("Turtall", "rpm",       new Farge(0, 0, 255), null, 0);
            oppsett[1] = new Oppsett("Temperatur dyno", "C", new Farge(128, 0, 0), null, 0);
            oppsett[2] = new Oppsett("Annet", "raw",         new Farge(0, 128, 128), null, 0);
            oppsett[3] = new Oppsett("Temperatur motor", "C", new Farge(255, 0, 0), null, 0);
            oppsett[4] = new Oppsett("Strøm", "A",           new Farge(128, 128, 0), null, 0);
            oppsett[5] = new Oppsett("Spenning", "Volt",     new Farge(0, 255, 128), null, 0);
            return oppsett;
        }
    }
}
