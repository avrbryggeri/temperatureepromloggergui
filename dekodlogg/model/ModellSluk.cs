﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class ModellSluk : IKombulator
    {
        private IModel model;
        
        public ModellSluk(IModel model) {
            this.model = model;
        }

        public List<double>[] Prosseser(List<double>[] verdier)
        {
            model.LeggTilVerdier(verdier);
            return verdier;
        }

        public IOppsett[] SettOpp(IOppsett[] oppsett)
        {
            model.SettOpp(oppsett);
            return oppsett;
        }
    }
}
