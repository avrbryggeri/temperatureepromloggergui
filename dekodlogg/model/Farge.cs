﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    public class Farge
    {
        public byte R;
        public byte G;
        public byte B;
        public Farge(byte r, byte g, byte b) {
            R = r;
            G = g;
            B = b;
        }
    }
}
