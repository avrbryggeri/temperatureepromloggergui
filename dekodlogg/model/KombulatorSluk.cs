﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.model
{
    class KombulatorSluk : IKombulator
    {
        private List<double>[] verdier;
        private IOppsett[] oppsett;

        public List<double>[] Prosseser(List<double>[] verdier)
        {
            this.verdier = verdier;
            return this.verdier;
        }

        public IOppsett[] SettOpp(IOppsett[] oppsett)
        {
            this.oppsett = oppsett;
            return this.oppsett;
        }

        public List<double>[] FaaVerdier() {
            return verdier;
        }

        public IOppsett[] FaaOppsett() {
            return oppsett;
        }
    }
}
