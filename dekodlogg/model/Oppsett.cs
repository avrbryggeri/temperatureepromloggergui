﻿using dekodlogg.interfaces;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    public class Oppsett : IOppsett
    {
        public string Navn { get; set; }
        public string Enhet { get;  set; }
        public Farge Farge { get;  set; }

        public double[] Skalering { get; set; }

        public double Forsinkelse { get; set; }

        public bool SecondaryAxis { get; set; }

        private static Random rnd = new Random();

        public Oppsett(string navn, string enhet, Farge farge, double[] skalering, double forsinkelse, bool secondaryAxis=false) {
            Navn = navn;
            Enhet = enhet;
            Farge = farge;
            Skalering = skalering;
            Forsinkelse = forsinkelse;
            SecondaryAxis = secondaryAxis;
        }
        public override string ToString()
        {
            return Navn + " [" + Enhet + "]";
        }
        
        public static Oppsett FromString(string s) {
            
            var r = Regex.Match(s, @"(.+) ?\[(.+)\]");

            return new Oppsett(r.Groups[1].ToString().Trim(), r.Groups[2].ToString().Trim(), new Farge((byte)((rnd.Next() & 0x7F)+0x80), (byte)((rnd.Next() & 0x7F) + 0x80), (byte)((rnd.Next() & 0x7F) + 0x80)), new double[] { 0.0, 1.0}, 0);
        }
    }
}
