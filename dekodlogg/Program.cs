﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dekodlogg.prosessering;
using dekodlogg.interfaces;

namespace dekodlogg
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {            
            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GUIPlot gp = new GUIPlot();
            Model model = new Model(gp);
            Application.Run(gp);
        }
    }
}
