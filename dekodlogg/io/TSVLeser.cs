﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using dekodlogg.prosessering;
using System.Windows.Forms;

namespace dekodlogg.interfaces
{
    public class TSVLeser : IDekoder
    {

        private Oppsett[] oppsett;
        private List<double>[] values;
        public TSVLeser(string filename, double dt, char separator)
        {
            ProcessFile(filename, dt, separator);
        }

        public void ProcessFile(string filename, double dt, char separator)
        {
            values = null;
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    // les header fra fil
                    if (sr.Peek() >= 0)
                    {
                        string[] split = sr.ReadLine().Split(separator);
                        values = new List<double>[split.Length];

                        oppsett = new Oppsett[split.Length - 1];
                        for (int i = 0; i < split.Length; i++)
                        {
                            values[i] = new List<double>();
                        }
                        for (int i = 0; i < oppsett.Length; i++)
                        {
                            oppsett[i] = Oppsett.FromString(split[i + 1]);

                        }
                    }
                    // less resten av fil
                    while (sr.Peek() >= 0)
                    {

                        string[] split = sr.ReadLine().Split(separator);
                        for (int i = 0; i < split.Length; i++)
                        {
                            double val = double.NaN;
                            try
                            {
                                val = double.Parse(split[i]);
                            }
                            catch
                            {

                            }
                            values[i].Add(val);
                        }
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Noe gikk galt ved åpning av " + filename + ": " + ex.Message);
            }
        }

        public IOppsett[] FaaOppsett()
        {
            return oppsett;
        }

        public List<double>[] FaaVerdier()
        {
            return values;
        }
    }
}