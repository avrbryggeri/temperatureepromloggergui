﻿using System;
using System.IO;
using dekodlogg.prosessering;
using System.Collections.Generic;

namespace dekodlogg.interfaces
{
    public class EEPROMFilDekoder : IDekoder
    {
        private double dt;
        private string filename;
        private int format;
        private IDekoder lu;

        public EEPROMFilDekoder(string filename, double dt, int format)
        {
            this.filename = filename;
            this.dt = dt;
            this.format = format;
            switch (format)
            {
                case 0:
                    lu = new LoggUtpakker(File.ReadAllBytes(filename), dt);
                    break;
                case 1:
                    lu = new LoggUtpakkerHighRes(File.ReadAllBytes(filename), dt);
                    break;
                default:
                    lu = new LoggUtpakker(File.ReadAllBytes(filename), dt);
                    break;
            }
        }

        public IOppsett[] FaaOppsett()
        {
            return lu.FaaOppsett();
        }

        public List<double>[] FaaVerdier()
        {
            return lu.FaaVerdier();
        }
    }
}