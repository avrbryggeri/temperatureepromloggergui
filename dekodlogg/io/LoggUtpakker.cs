﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dekodlogg.prosessering;

namespace dekodlogg
{
    class LoggUtpakker : IDekoder
    {
        private byte[] logg;
        private int logIdx;

        List<double> times = null;
        List<double> values = null;
        double dt;

        public LoggUtpakker(byte[] logg, double dt)
        {
            this.logg = logg;
            logIdx = 0;
            this.dt = dt;
        }

        int GetNibble()
        {
            int ret;
            if ((logIdx & 1) == 0)
            {
                ret = (logg[logIdx >> 1] >> 4) & 0xF;
            }
            else
            {
                ret = logg[logIdx >> 1] & 0xF;
            }
            logIdx++;
            return ret;
        }

        private void DecompressLog() {
            int current = 0;
            double time = 0;
            while ((logIdx >> 1) < logg.Length)
            {
                int nibble = GetNibble();
                if ((nibble & 0x8) == 0)
                { // diff verdi
                    if ((nibble & 0x7) > 3)
                    { // negativ diff
                        current += -8 + (nibble & 0x7);
                    }
                    else
                    {
                        current += (nibble & 0x7);
                    }
                    times.Add(time);
                    values.Add(current);
                    time += dt;
                }
                else
                {
                    if ((nibble & 0x4) == 4)
                    { // repetisjon
                        int reps = ((nibble & 0x3) << 4) | GetNibble();
                        for (int i = 0; i < reps; i++)
                        {
                            times.Add(time);
                            values.Add(current);
                            time += dt;
                        }
                    }
                    else
                    {
                        int new_value = ((nibble & 0x3) << 4) | GetNibble();
                        current = new_value;
                        times.Add(time);
                        values.Add(current);
                        time += dt;
                    }
                }
            }
        }

        public List<double>[] FaaVerdier() {
            List<double>[] verdier = new List<double>[2];
            if (values == null) {
                times = new List<double>();
                values = new List<double>();
                verdier[0] = times;
                verdier[1] = values;
                DecompressLog();
            }
            
            return verdier;
        }

        public IOppsett[] FaaOppsett()
        {
            return KurveOppsett.TemperaturOppsett();
        }
    }
}
