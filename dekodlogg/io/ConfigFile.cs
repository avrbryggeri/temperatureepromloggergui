﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.io
{
    class ConfigFile
    {
        private Dictionary<string, string> map;
        string filename;
        private static ConfigFile instance = new ConfigFile();

        public static ConfigFile Inst() {
            return instance;
        }

        private ConfigFile() {
            map = new Dictionary<string, string>();
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string logpath = Path.Combine(path, "BryggeriGUI");
            Directory.CreateDirectory(logpath);
            filename = logpath + Path.DirectorySeparatorChar + "configuration.txt";
            Load();
        }

        private void Load() {
            try
            {
                string[] lines = System.IO.File.ReadAllLines(filename);
                foreach (string line in lines)
                {
                    var kv = line.Split(new string[]{ ": "}, StringSplitOptions.None);
                    map[kv[0].ToLower()] = kv[1];
                }
            }
            catch {

            }
        }

        private void Save() {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, false))
            {
                foreach (var kv in map) {
                    file.WriteLine(kv.Key + ": " + kv.Value);
                }
            }
        }

        public string Get(string key) {
            if (map.ContainsKey(key.ToLower()))
            {
                return map[key.ToLower()];
            }
            else {
                return "";
            }
        }

        public void Set(string key, string value) {
            map[key.ToLower()] = value;
            Save();
        }
    }
}
