﻿using System;
using System.Collections.Generic;
using dekodlogg.prosessering;
using System.IO;

namespace dekodlogg.interfaces
{
    internal class SDCardDekoder : IDekoder
    {
        private double dt;
        private string filename;
        private List<double>[] verdier;
        public SDCardDekoder(string filename, double dt)
        {
            this.filename = filename;
            this.dt = dt;
            byte[] log = File.ReadAllBytes(filename);
            double time = 0;
            double verdi = 0;
            verdier = new List<double>[2];
            verdier[0] = new List<double>();
            verdier[1] = new List<double>();
            int word;
            for (int i = 0; i < log.Length / 2; i++) {
                word = BitConverter.ToInt16(log, 2 * i);
                    //log[2 * i] + (log[2 * i+1] << 8);
                if ((word != 0) && (word != 0xFFFF))
                {
                    verdi = (word) / 16.0;
                    verdier[0].Add(time);
                    verdier[1].Add(verdi);
                    time += dt;
                }
            }
        }

        public IOppsett[] FaaOppsett()
        {
            return KurveOppsett.TemperaturOppsett();
        }

        public List<double>[] FaaVerdier()
        {
            return verdier;       }
    }
}