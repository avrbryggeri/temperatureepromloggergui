﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.io {
        class TSVLiveSkriver : IDisposable {
                StreamWriter outputFile;
                string separator;
                public TSVLiveSkriver(string filnavn, IOppsett[] kurver, string tidslabel, string separator) {
                        outputFile = new StreamWriter(filnavn);
                        this.separator = separator;
                        outputFile.Write(tidslabel);
                        for (int i = 0; i < kurver.Length; i++) {
                                outputFile.Write(separator);
                                outputFile.Write(kurver[i].ToString());
                        }
                        outputFile.WriteLine();
                }

                public void Dispose() {
                        Close();
                }

                public void LagreVerdier(List<double>[] verdier) {
                        for (int i = 0; i < verdier[0].Count; i++) {
                                outputFile.Write(verdier[0][i]); // tid
                                for (int j = 1; j < verdier.Length; j++) // resten
                                {
                                        outputFile.Write(separator);
                                        outputFile.Write(verdier[j][i]);
                                }
                                outputFile.WriteLine();
                        }
                        outputFile.FlushAsync();
                }

                public void Close() {
                        if (outputFile != null) {
                                outputFile.Flush();
                                outputFile.Close();
                                ((IDisposable)outputFile).Dispose();
                                outputFile = null;
                        }
                }

        }
}
