﻿using dekodlogg.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class TSVSkriver : IFilskriver
    {
        char separator;
        public TSVSkriver(char separator)
        {
            this.separator = separator;
        }
        public void Lagre(string filnavn, IOppsett[] kurver, List<double>[] verdier, string tidslabel)
        {
            if (verdier == null) return;

            using (StreamWriter outputFile = new StreamWriter(filnavn))
            {
                outputFile.Write(tidslabel);
                for (int i = 0; i < kurver.Length; i++)
                {
                    outputFile.Write(separator);
                    outputFile.Write(kurver[i].ToString());
                }
                outputFile.WriteLine();
                for (int i = 0; i < verdier[0].Count; i++)
                {
                    outputFile.Write(verdier[0][i]); // tid
                    for (int j = 1; j < verdier.Length; j++) // resten
                    {
                        outputFile.Write(separator);
                        outputFile.Write(verdier[j][i]);
                    }
                    outputFile.WriteLine();
                }
            }
        }
    }
}
