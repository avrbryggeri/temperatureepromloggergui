﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.prosessering
{
    class Crc8
    {
        public byte Crc { get; set; }
        public byte Poly { get; set; }

        public Crc8(byte poly)
        {
            Poly = poly;
        }

        public void Reset() {
            Crc = 0;
        }
        public void Process(byte c)
        {
            for (int i = 0; i < 8; i++)
            {
                byte sum = (byte)(((Crc) ^ c) & 0x01);
                Crc >>= 1;
                if (sum != 0)
                {
                    Crc ^= Poly;
                }
                c >>= 1;
            }
        }
        public void Process(byte[] bytes) {
            foreach (var b in bytes) {
                Process(b);
            }
        }
    }
}
