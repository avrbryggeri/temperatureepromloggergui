﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.trykktemp
{
   
    public class PackageDecoder
    {
        int state;
        Crc8 crc;
        int length;
        int type;
        List<byte> payload;
        public PackageDecoder()
        {
            this.state = 0;
            this.crc = new Crc8(0x8C);
            payload = new List<byte>();
        }
        public Package GetPackage() {
            return new Package(type, payload.ToArray());
        }
        public bool ProcessByte(byte b) {
            switch (state) {
                case 0: // wait for preamble
                    if (b == 0x55) {
                        state = 1;
                    }
                    break;
                case 1: // length
                    length = b;
                    crc.Reset();
                    state = 2;
                    break;
                case 2: // type
                    type = b;
                    crc.Process(b);
                    payload.Clear();
                    state = 3;
                    break;
                case 3: // payload
                    payload.Add(b);
                    crc.Process(b);
                    if (payload.Count == length) {
                        state = 4;
                    }
                    break;
                case 4: // checksum
                    if (crc.Crc == b)
                    {
                        state = 0;
                        return true;
                    }
                    else {
                        Console.WriteLine("Wrong checksum: " + crc.Crc + " != " + b);
                    }
                    break; 
                default:
                    state = 0;
                    break;
            }
            return false;
        }
    }
}
