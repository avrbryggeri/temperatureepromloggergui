﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.trykktemp
{
    public class Package
    {
        public int Type { get; }
        public byte[] Payload { get; }

        public Package(int type, byte[] payload)
        {
            Type = type;
            Payload = payload == null ? new byte[0] : payload;
        }
    }
}
