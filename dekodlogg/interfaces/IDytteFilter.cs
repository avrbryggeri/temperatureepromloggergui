﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IDytteFilter
    {
        double[] Filtrer(double[] inn);
        void Nullstill();
    }
}
