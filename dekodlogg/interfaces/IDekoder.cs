﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IDekoder
    {
        List<double>[] FaaVerdier();
        IOppsett[] FaaOppsett();
    }
}
