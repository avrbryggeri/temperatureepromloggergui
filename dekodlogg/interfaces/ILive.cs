﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface ILive
    {
        bool Start(IModel model);
        bool Stopp();
        bool ErStartet();
    }
}
