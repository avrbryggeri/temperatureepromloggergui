﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IKombulator
    {
        IOppsett[] SettOpp(IOppsett[] oppsett);
        List<double>[] Prosseser(List<double>[] verdier);
    }
}
