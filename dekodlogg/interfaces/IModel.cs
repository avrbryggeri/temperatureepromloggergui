﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IModel
    {
        void SettOpp(IOppsett[] oppsett);
        void LeggTilVerdier(List<double>[] grafer);
        void LagreTilFil(string filnavn);
        void LesFraFil(string fileName, double value);
        void SettKombulator(IKombulator kombulator);
        List<double>[] FaaVerdier();
        IOppsett[] FaaOppsett();
    }
}
