﻿using dekodlogg.prosessering;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IOppsett
    {
        string Navn { get; }
        string Enhet { get; }
        double[] Skalering { get; } // null = 1 i gain, 0 i offset. Ellers: Sum av (Verdi^k)*Skalering[k]
        double Forsinkelse { get; }
        Farge Farge { get; }
        bool SecondaryAxis { get; }
    }
}
