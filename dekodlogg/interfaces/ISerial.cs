﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public delegate void DataReceived(byte[] data);

    public interface ISerial
    {
        DataReceived DataReceived { get; set; }
        void Write(byte[] buf);
    }
}
