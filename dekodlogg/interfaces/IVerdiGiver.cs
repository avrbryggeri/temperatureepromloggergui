﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IVerdiGiver
    {
        bool Start();
        bool Stopp();
        bool Aktiv();
        void SettModel(Model model);
    }
}
