﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IBrukerGrensesnitt
    {
        void SettModel(IModel model);
        void SettOpp(IOppsett[] kurver);
        void PlotVerdier(List<double>[] verdier);
    }
}
