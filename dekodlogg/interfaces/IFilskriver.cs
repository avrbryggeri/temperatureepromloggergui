﻿using dekodlogg.prosessering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dekodlogg.interfaces
{
    public interface IFilskriver
    {
        void Lagre(string filnavn, IOppsett[] oppsett, List<double>[] verdier, string tidslabel);
    }
}
